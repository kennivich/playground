creatures = new Mongo.Collection('creatures') as Collection<any>;

Meteor.methods({
    'creatures.kill'(creatureId) {
        creatures.remove({_id: creatureId});
    },
    'creatures.addBug'() {
        creatures.insert({
            type: 'Bug',
            position: {
                x: Math.random() * 1000 + 100,
                y: Math.random() * 500 + 100,
                angle: 0
            }
        });
    },
    'creatures.addGecko'() {
        creatures.insert({
            type: 'Gecko',
            position: {
                x: Math.random() * 1000 + 100,
                y: Math.random() * 500 + 100,
                angle: 0
            }
        });
    }
});