objects = new Mongo.Collection('objects') as Collection<object>;

var mapWidth = 1700;
var mapHeight = 746;
var objectSize = 24;


Meteor.methods({
    'objects.destroyObject'(objectId) {
        objects.remove({_id: objectId})
    },
    'objects.addRandomPositionObject'(type, number = 1) {
        if (Meteor.isServer)
            for (var i = 0; i < number; i++)
                addRandomPositionbject(type)
    },
    'objects.removeAll'() {
        if (Meteor.isServer)
            objects.remove({});
    }
});

Meteor.call('objects.removeAll');
//Walls
// Left Wall
var walls_num = mapHeight / objectSize;
var x = 0;
var y = objectSize / 2;
for(let i = 0; i < walls_num; i++){
    addObject('Stone', x, y);
    y += objectSize;
}
// Right Wall
x += mapWidth ;
y = objectSize / 2;
for(let i = 0; i < walls_num; i++){
    addObject('Stone', x, y);
    y += objectSize;
}
// Top Wall
walls_num = (mapWidth - objectSize) / objectSize;
x = objectSize;
y = objectSize / 2;
for(let i = 0; i < walls_num; i++){
    addObject('Stone', x, y);
    x += objectSize;
}

// Bottom Wall
walls_num = mapWidth / objectSize;
x = objectSize / 2;
y += mapHeight + objectSize;
for(let i = 0; i < walls_num; i++){
    addObject('Stone', x, y);
    x += objectSize;
}
// Random Stones
for(let i = 0; i < 70; i++){
    addRandomPositionbject("Stone")
}


function addObject(type, x, y) {
    objects.insert({
        type: type,
        x: x,
        y: y
    });
}

function addRandomPositionbject(type) {
    addObject(type, (Math.random() * (mapWidth - 2*objectSize) + objectSize), (Math.random() * (mapHeight - 2*objectSize) + objectSize))
}