export interface IMotorNeuronProps {
    name: string,
    action: (excitement: number) => void
}

interface IMotorNeuron extends IMotorNeuronProps{
    excitement: number
}

interface IMotorCortex {
    neurons: IMotorNeuron[],
    excite: (neuronName: string, value: number) => void
}

class MotorCortex implements IMotorCortex {
    neurons;
    constructor (neuronsProps: IMotorNeuronProps[]) {
        this.neurons = [];
        for (var neuronProps of neuronsProps) {
            this.neurons.push({name: neuronProps.name, action: neuronProps.action, excitement: 0});
        }
    };

    excite = (neuronName, value) => {
        this.neurons = this.neurons.map(function (neuron) {
            if (neuron.name == neuronName)
                neuron.excitement += value;
            return neuron
        });
    };

    dumpNeurons = () => {
        var
            normalizationValue = 0,
            normalizedActions = [];

        for (var neuron of this.neurons) {
            normalizationValue += Math.abs(neuron.excitement);
        }

        for (var neuron of this.neurons) {
            neuron.excitement /= normalizationValue;
            if ( Math.abs(neuron.excitement) > 0.01 )
                normalizedActions.push(neuron);
        }
        return normalizedActions;
    };

    resetExcitations = () => {
        this.neurons = this.neurons.map(function (neuron) {
            neuron.excitement = 0;
            return neuron
        });
    }
}

export default MotorCortex;