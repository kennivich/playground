/// <reference path="/typings/modules/react/index.d.ts" />
import * as React from 'react';

export default class MainLayout extends React.Component<{toolbox: any, enviroment: any}, {}> {
    render() {
        return (
            <div className="container">
                {this.props.toolbox}
                {this.props.enviroment}
            </div>
        );
    }
}
