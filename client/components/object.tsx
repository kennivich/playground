/// <reference path="/typings/modules/react/index.d.ts" />
import * as React from 'react';

export default class ObjectComponent extends React.Component<{object: any}, {}> {
    render() {
        return (
            <div className="object" id={this.props.object._id} key={this.props.object._id} type={this.props.object.type} style={this.props.object.styles}>
                <div className="object-content">
                    <i className="material-icons object-icon">nothing</i>
                </div>
            </div>
        );
    }
}