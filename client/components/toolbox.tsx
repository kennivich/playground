/// <reference path="/typings/modules/react/index.d.ts" />
import * as React from 'react';

class AddBugButton extends React.Component<{}, {}> {
    render() {
        return (
            <button onClick={this._handleClick} className="mdl-button mdl-js-button mdl-button--raised mdl-button--accent">
                Add Bug
            </button>
        );
    }
    _handleClick() {
        Meteor.call('creatures.addBug');
    }
}

class AddGeckoButton extends React.Component<{}, {}> {
    render() {
        return (
            <button onClick={this._handleClick} className="mdl-button mdl-js-button mdl-button--raised mdl-button--accent">
                Add Gecko
            </button>
        );
    }
    _handleClick() {
        Meteor.call('creatures.addGecko');
    }
}

class AddGrassButton extends React.Component<{}, {}> {
    render() {
        return (
            <button onClick={this._handleClick} className="mdl-button mdl-js-button mdl-button--raised mdl-button--accent">
                Add Grass
            </button>
        );
    }
    _handleClick() {
        Meteor.call('objects.addRandomPositionObject', 'Grass', 5);
    }
}

class AddAppleButton extends React.Component<{}, {}> {
    render() {
        return (
            <button onClick={this._handleClick} className="mdl-button mdl-js-button mdl-button--raised mdl-button--accent">
                Add Apples
            </button>
        );
    }
    _handleClick() {
        Meteor.call('objects.addRandomPositionObject', 'Apple', 3);
    }
}

export default class Toolbox extends React.Component<{}, {}>{
    render() {
        return (
            <div className="mdl-layout__header-row toolbox">
                <span className="mdl-layout-title">Playground</span>
                <div className="mdl-layout-spacer"></div>
                <nav className="mdl-navigation">
                    <AddBugButton/>
                    <AddGeckoButton/>
                    <AddGrassButton/>
                    <AddAppleButton/>
                </nav>
            </div>
        );
    }
}

