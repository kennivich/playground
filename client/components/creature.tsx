/// <reference path="/typings/modules/react/index.d.ts" />
import * as React from 'react';

class CreatureComponent extends React.Component<creatureProps, any> {
    constructor() {
        super();
        this.state = {
            styles: {
                transform: 'none'
            },
            size: 32,
            acting: false,
            energy: 100
        };
        
        this.state.updateStyles = () => {
            const shiftAngle = 90;
            this.state.styles = {
                top: Math.round(this.state.position.y - (this.state.size / 2)) + 'px',
                left: Math.round(this.state.position.x - (this.state.size / 2)) + 'px',
                transform: 'rotate(' + Math.round(this.state.position.angle + shiftAngle) + 'deg)'
            };
        }
    }

    render () {
        return (
            <div className="creature" type={this.props.type} style={{top: this.state.styles.top, left: this.state.styles.left}}>
                <i className="material-icons" style={{transform: this.state.styles.transform}}>creature</i>
                <progress value={this.state.energy} max="100"></progress>
            </div>

        );
    }
}

export default CreatureComponent;