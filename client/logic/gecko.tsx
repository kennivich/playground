/// <reference path="/typings/modules/react/index.d.ts" />
import * as React from 'react';
import Creature from './creature.tsx'
import MotorCortex from '/client/cortex/motorCortex.tsx'

class Gecko extends Creature {
    constructor() {
        super();
        this.state.eatingRadius = 30;
        this.state.smellRadius = 200;
        this.state.viewingDistance = 400;
        this.state.viewingAngle = 90;
        this.state.speed = 10;
        this.state.edible = ['Apple', 'Bug'];
        this.state.avoidable = ['Stone'];
        
        this.state.motorCortex = new MotorCortex([
            {name: 'go', action: this.go},
            {name: 'rotate', action: this.rotate},
            {name: 'eat', action: this.eat}
        ] as any);
    }
}

export default Gecko;