/// <reference path="/typings/modules/react/index.d.ts" />
import * as React from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import ObjectComponent from '/client/components/object.tsx'
import Bug from './bug.tsx'
import Gecko from './gecko.tsx'
import Creature from "./creature";

class Enviroment extends React.Component <{objects: any, creatures: any}, {}>{
    static propTypes = {};
    constructor () {
        super();
    }

    render() {
        return (
            <div className="map">
                <div className="objects-pool">
                    {this.renderObjects()}
                    {this.renderCreatures()}
                </div>
            </div>
        );
    }
    renderCreatures() {
        const availableSenses = {
            smell: this.getSmells,
            look: this.getVisibleObjects
        };
        const availableActions = {
            eat: this.eat
        };

        return this.props.creatures.map((creature) => {
            if (creature.type == 'Bug')
                return <Bug type="Bug" availableSenses={availableSenses} availableActions={availableActions} position={creature.position} key={creature._id} id={creature._id} ref={creature._id}/>
            if (creature.type == 'Gecko')
                return <Gecko type="Gecko" availableSenses={availableSenses} availableActions={availableActions} position={creature.position} key={creature._id} id={creature._id} ref={creature._id}/>
        })
    }
    renderObjects() {
        return this.props.objects.map((object) => {
            object.styles = {
                top: Math.floor(object.y) - 12 + 'px',
                left: Math.floor(object.x) - 12 + 'px',
                color: object.color
            };
            return <ObjectComponent key={Math.idGenerator()} object={object}/>;
        });
    }

    eat = (creatureState): perception[] => {
        var food = [];
        const nearObjects = this.getNearObjects(creatureState, creatureState.eatingRadius);

        for (var object of nearObjects) {


            if (_.contains(creatureState.edible, object.type)) {
                //console.log(creatureState.edible);
                food.push({
                    callories: 1
                });
                this.destroyObject(object._id);
            }
        }

        return food;
    };

    getSmells = (creatureState): smell[] => {
        var
            smells = [],
            distance;

        const nearObjects = this.getNearObjects(creatureState, creatureState.smellRadius);
        if (!nearObjects) return [];

        for (var object of nearObjects) {
            distance = Math.distance(creatureState.position, object);

            if (object.type != 'Stone') {
                smells.push({
                    type: object.type,
                    rotation: Math.rotationToObject(creatureState.position, object),
                    isNear: (distance < 30),
                    intensity: 1.0 - (distance / creatureState.smellRadius)
                });
            }
        }
        return smells
    };
    getVisibleObjects = (creatureState): visibleObject[] => {
        var
            visibleObjects = [],
            distance;

        const nearObjects = this.getNearObjects(creatureState, creatureState.viewingDistance);
        if (!nearObjects)
            return [];

        for (var object of nearObjects) {
            const inViewingField = Math.abs(Math.rotationToObject(creatureState.position, object)) <= creatureState.viewingAngle;
            if (inViewingField) {
                distance = Math.distance(creatureState.position, object);
                visibleObjects.push({
                    type: object.type,
                    rotation: Math.rotationToObject(creatureState.position, object) / 180,
                    isNear: (distance < 30),
                    intensity: 1.0 - (distance / creatureState.viewingDistance)
                });
            }
        }
        return visibleObjects
    };
    getNearObjects = (creatureState, radius): any[] => {
        var nearObjects = [];
        for (var object of this.props.objects) {
            if (Math.distance(creatureState.position, object) <= radius)
                nearObjects.push(object);
        }

        for (var creatureId in this.refs) {
            const creature = this.refs[creatureId] as Creature;
            const distance = Math.distance(creatureState.position, creature.state.position);
            if ( distance <= radius && distance > 1 )
                nearObjects.push({_id: creatureId, type: creature.props.type, x: creature.state.position.x, y: creature.state.position.y, })
        }

        return nearObjects == [] ? null : nearObjects
    };

    destroyObject = (objectId) => {
        Meteor.call('objects.destroyObject', objectId);
        Meteor.call('creatures.kill', objectId);
    }
}

Enviroment.propTypes = {
    objects: React.PropTypes.array.isRequired,
    creatures: React.PropTypes.array.isRequired
};

export default createContainer(() => {
    Meteor.subscribe('objects');
    Meteor.subscribe('creatures');
    return {
        objects: objects.find({}).fetch(),
        creatures: creatures.find({}).fetch()
    };
}, Enviroment);