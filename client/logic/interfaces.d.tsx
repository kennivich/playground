///<reference path="/typings/modules/react/index.d.ts" />
import Collection = Mongo.Collection;
declare var objects: Collection<object>;
declare var creatures: Collection<any>;
declare var FlowRouter: any;
declare var _: any;

interface IPoint {
    x: number,
    y: number
}
interface IVec extends IPoint{
    angle: number
}

interface object extends IPoint{
    type: string,
    _id?: string
}

interface IPerception {
    type: string,
    isNear?: number,
    intensity?: number,
    distance?: number,
    rotation?: number
}
interface smell extends IPerception {}
interface visibleObject extends IPerception {
    distance: number
}
interface food {
    callories: number
}
type perception = smell | visibleObject;

interface availableSenses {
    smell(bugState): perception[],
    look(bugState): perception[]
}

interface availableActions {
    eat(bugState): food[]
}

interface creatureProps {
    id: string,
    type: string,
    availableSenses: availableSenses,
    availableActions: availableActions,
    position: IVec
}

