interface Math {
    idGenerator(): string,
    degrees(radians: number): number,
    radians(degrees: number): number,
    distance(object1: IPoint, object2: IPoint): number,
    sinDeg(angle: number): number,
    cosDeg(angle: number): number,
    rotationToObject(creaturePosition: IVec, object: IPoint): number,
    sign(x: number): number
}

Math.idGenerator = function () {
    var S4 = function() {
        return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
    };
    return (S4()+S4()+S4()+S4());
};

Math.degrees = function(radians) {
    return radians * 180 / Math.PI;
};

Math.radians = function(degrees) {
    return degrees * Math.PI / 180;
};

Math.sinDeg = function (angle) {
    return Math.sin(Math.radians(angle));
};

Math.cosDeg = function (angle) {
    return Math.cos(Math.radians(angle));
};

Math.distance = function (obj1, obj2) {
    return Math.sqrt(Math.pow(obj1.x - obj2.x, 2) + Math.pow(obj1.y - obj2.y, 2));
};

Math.rotationToObject = function (creaturePosition, object) {
    const offset = {
        x: -creaturePosition.x + object.x,
        y: -creaturePosition.y + object.y
    };

    let rotationToObject = Math.degrees(Math.atan(offset.y / offset.x));

    if (offset.x < 0  && offset.y < 0)
        rotationToObject -= 180;
    if (offset.x < 0  && offset.y > 0)
        rotationToObject += 180;

    rotationToObject -= creaturePosition.angle;
    if (rotationToObject < -180)
        rotationToObject += 360;
    if (rotationToObject > 180)
        rotationToObject -= 360;

    return rotationToObject;
};

interface Array {
    extend: any
}

Array.prototype.extend = function (other_array) {
    other_array.forEach(function(v) {this.push(v)}, this);
};