/// <reference path="/typings/modules/react/index.d.ts" />
import * as React from 'react';
import CreatureComponent from '/client/components/creature.tsx';

class Creature extends CreatureComponent {
    componentDidMount = () => {
        this.state.timer = setInterval(this.updateState, 30);
        this.state.position = this.props.position;
    };

    componentWillUnmount = () => {
        clearInterval(this.state.timer);
    };

    updateState = () => {
        if (this.state.acting)
            return;

        if (this.state.energy <= 0)
            this.die();

        this.motorCortexActivations(this.getPerceptions());
        this.performActions();
        this.state.updateStyles();
    };
    getPerceptions = () => {
        const availableSenses = this.props.availableSenses;
        var perceptions = [];
        for (var sense in availableSenses) {
            perceptions.extend(
                availableSenses[sense](this.state)
            );
        }
        return perceptions
    };

    // Default behavior
    motorCortexActivations = (perceptions: perception[]) => {

        this.state.motorCortex.excite('go', 0.5);

        for ( var perception of perceptions ) {

            // Rotate and go to food
            if ( _.contains(this.state.edible, perception.type )) {
                this.state.motorCortex.excite('rotate', perception.intensity * Math.sign(perception.rotation));
                this.state.motorCortex.excite('go', perception.intensity);
                if ( perception.isNear )
                    this.state.motorCortex.excite('eat', 0.1);
            }

            // Avoid stones
            if ( perception.type == 'Stone' ) {
                this.state.motorCortex.excite('rotate', -perception.intensity * Math.sign(perception.rotation) * 0.2);
                if ( Math.abs(perception.rotation) < 90 && perception.isNear ) {
                    this.state.motorCortex.excite('rotate', -perception.intensity * Math.sign(perception.rotation) * 5);
                    this.state.motorCortex.excite('go', -0.3);
                }
            }
        }
    };

    performActions = () => {
        const actions = this.state.motorCortex.dumpNeurons();

        this.state.acting = true;
        for ( var {action: action, excitement: excitement} of actions ) {
            action(excitement);
            this.state.energy -= 0.05;
        }
        this.state.acting = false;
        this.state.motorCortex.resetExcitations();
    };
    go = (excitation) => {
        const shiftX = this.state.speed * Math.cosDeg(this.state.position.angle) * excitation;
        const shiftY = this.state.speed * Math.sinDeg(this.state.position.angle) * excitation;
        this.setState({
            position: {
                x: this.state.position.x + shiftX,
                y: this.state.position.y + shiftY,
                angle: this.state.position.angle
            }
        });
    };
    rotate = (excitation) => {
        this.setState({
            position: {
                x: this.state.position.x,
                y: this.state.position.y,
                angle: (this.state.position.angle + this.state.speed * excitation) % 360
            }
        });
    };
    eat = () => {
        if (!this.props.availableActions.eat) return;

        const food = this.props.availableActions.eat(this.state);
        if ( food.length )
            this.state.energy = 100;
    };
    die = () => {
        clearInterval(this.state.timer);
        setTimeout( () => { Meteor.call('creatures.kill', this.props.id) }, 1000);
    };
    respawn = () => {
        this.forceUpdate();
        this.state.timer = setInterval(this.updateState, 20);
        this.state.energy = 100;
    }
}

export default Creature;