/// <reference path="/typings/modules/react/index.d.ts" />
import * as React from 'react';
import Creature from './creature.tsx'
import MotorCortex from '/client/cortex/motorCortex.tsx'

class Bug extends Creature {
    constructor() {
        super();
        this.state.eatingRadius = 20;
        this.state.smellRadius = 100;
        this.state.viewingDistance = 300;
        this.state.viewingAngle = 90;
        this.state.speed = 5;
        this.state.edible = ['Grass'];
        this.state.avoidable = ['Stone', 'Gecko'];

        this.state.motorCortex = new MotorCortex([
            {name: 'go', action: this.go},
            {name: 'rotate', action: this.rotate},
            {name: 'eat', action: this.eat}
        ] as any);
    }
}

export default Bug;