import * as React from 'react';
import {mount} from 'react-mounter';
import MainLayout from '/client/layouts/mainLayout.tsx';
import Toolbox from '/client/components/toolbox.tsx';
import Enviroment from '/client/logic/enviroment.tsx';

FlowRouter.route("/", {
    action () {
        mount(MainLayout, {
            toolbox: <Toolbox/>,
            enviroment: <Enviroment/>,
        });
    }
});